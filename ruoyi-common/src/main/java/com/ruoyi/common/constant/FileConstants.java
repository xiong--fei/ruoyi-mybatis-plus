package com.ruoyi.common.constant;

/**
 * 文件常量信息
 *
 * @author fansd
 */
public class FileConstants {
    /**
     * 图片类型
     */
    public static final String IMAGE_TYPE_JPG = "JPG";
    public static final String IMAGE_TYPE_PNG = "PNG";
    public static final String IMAGE_TYPE_GIF = "GIF";
    public static final String IMAGE_TYPE_BMP = "BMP";


    /**
     * 文件类型 json
     */
    public static final String FILE_TYPE_JSON = "json";

    /**
     * 文件类型 xml
     */
    public static final String FILE_TYPE_XML = "xml";

    /**
     * XML 后缀名
     */
    public static final String XML_SUFFIX = ".xml";

    /**
     * json 后缀名
     */
    public static final String JSON_SUFFIX = ".json";

    /**
     * 上一级目录命令
     * linux、window 使用 cd..
     */
    public static final String SUPERIOR_DIRECTORY_COMMON = "..";


}
