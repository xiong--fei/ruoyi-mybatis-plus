package com.ruoyi.common.annotation;

import java.lang.annotation.*;

/**
 * 自定义树形结构注解
 *
 * @author fansd
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Tree {
    /**
     * 树形结构的类型
     */
    public String treeCategory() default "";
}
