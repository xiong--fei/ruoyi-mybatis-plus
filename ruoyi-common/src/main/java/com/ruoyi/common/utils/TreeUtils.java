package com.ruoyi.common.utils;

import com.ruoyi.common.constant.CharConstants;
import com.ruoyi.common.constant.TreeConstants;
import com.ruoyi.common.core.domain.TreeEntity;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.domain.tree.TreeNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 字典工具类
 *
 * @author fansd
 */
public class TreeUtils {

    private static String cacheKey = null;

    public TreeUtils(String key) {
        cacheKey = key;
    }

    /**
     * 设置cache key
     *
     * @param entities 参数键
     * @return 缓存键key
     */
    public List<Object> buildTree(List<?> entities) {

        List<Object> returnList = new ArrayList<>();


        for (Iterator<?> iterator = entities.iterator(); iterator.hasNext(); ) {
            Object obj = iterator.next();
            TreeEntity entity = (TreeEntity) obj;

            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (TreeConstants.PARENT_ROOT_VALUE.equals(entity.getPid())) {
                recursionFn(entities, obj);
                returnList.add(obj);
            }
        }

        if (returnList.isEmpty()) {
            entities.forEach(item -> {
                returnList.add(item);
            });
        }

        // 存储树形数据到redis
//        initCacheTreeData(returnList);
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<?> list, Object node) {
        TreeEntity treeEntity = (TreeEntity) node;
        // 得到子节点列表， 并赋值给node节点
        List<?> childList = getChildList(list, node);
        treeEntity.setChildren(childList);

        // 注入到redis中
//        initCacheTreeItemData(node);

        // 递归遍历
        for (Object tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<?> getChildList(List<?> list, Object t) {
        TreeEntity treeEntity = (TreeEntity) t;
        List<Object> tlist = new ArrayList<Object>();
        Iterator<?> it = list.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            TreeEntity n = (TreeEntity) obj;
            if (StringUtils.isNotNull(n.getPid()) && n.getPid().equals(treeEntity.getId())) {
                tlist.add(obj);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<?> list, Object t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }

    /**
     * 初始化树形数据各节点到redis缓存中
     */
    private void initCacheTreeItemData(TreeNode node) {
        // 拼接缓存主键
        String pk = StringUtils.join(CharConstants.COLON, cacheKey, node.getId());
        // 存储到redis
        SpringUtils.getBean(RedisCache.class).setCacheObject(pk, node);
    }

    /**
     * 树形结构数据到redis缓存中
     */
    private void initCacheTreeData(List<TreeNode> node) {
        // 存储到redis
        SpringUtils.getBean(RedisCache.class).setCacheObject(cacheKey, node);
    }

    /**
     * 层次自增
     */
    public static Integer compareLevelDepth(TreeNode entity) {
        return entity.getLevelDepth() + 1;
    }


    /**
     * 获取下一个编码
     */
    public static String genNextLevelCode(String maxLevelCode, String parentLevelCode, String initCode, Integer step) {

        String nextCode = "";

        if (StringUtils.isNotEmpty(maxLevelCode)) {
            List<String> levelCodeList = Stream.of(maxLevelCode.split("\\" + TreeConstants.LAYOUT_CODE_SEPARATOR)).collect(Collectors.toList());
            String lastLevelCode = null;

            if (levelCodeList.size() > 0) {
                lastLevelCode = String.valueOf(Integer.valueOf(levelCodeList.get(levelCodeList.size() - 1)) + step);
            } else {
                lastLevelCode = String.valueOf(Integer.valueOf(maxLevelCode) + step);
            }
            nextCode = maxLevelCode.substring(0, maxLevelCode.length() - lastLevelCode.length()) + lastLevelCode;

        } else {
            nextCode = (StringUtils.isEmpty(parentLevelCode) ? "" : parentLevelCode + TreeConstants.LAYOUT_CODE_SEPARATOR) + initCode;
        }
        return nextCode;
    }

    /**
     * 获取层次
     */
    public static Integer genLevelDepth(String levelCode) {
        return levelCode.split("\\" + TreeConstants.LAYOUT_CODE_SEPARATOR).length;
    }

    /**
     * 获取排序
     */
    public static Integer genOrderNum(String levelCode) {
        List<String> levelCodeList = Stream.of(levelCode.split("\\" + TreeConstants.LAYOUT_CODE_SEPARATOR)).collect(Collectors.toList());
        if (levelCodeList.size() > 0) {
            return Integer.valueOf(levelCodeList.get(levelCodeList.size() - 1));
        } else {
            return Integer.valueOf(levelCode);
        }
    }

    /**
     * 获取编码
     */
    public static String genCode(String levelCode) {
        return levelCode.replace(TreeConstants.LAYOUT_CODE_SEPARATOR, "");
    }

    /**
     * 获取编码
     */
    public static boolean isParentNode(String pLevelCode, String levelCode) {
        Integer pLen = pLevelCode.length();
        Integer len = levelCode.length();

        if (pLen >= len) {
            return false;
        }

        if (levelCode.startsWith(pLevelCode)) {
            pLevelCode += TreeConstants.LAYOUT_CODE_SEPARATOR;
            String temp = levelCode.replace(pLevelCode, "");
            String[] temps = temp.split("\\" + TreeConstants.LAYOUT_CODE_SEPARATOR);

            if (temps.length > 1) {
                return false;
            }
        }
        return true;
    }
}
