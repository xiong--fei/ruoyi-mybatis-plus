import request from '@/utils/request'

// demo6 查询
export function listChapter6(query) {
  return request({
    url: '/activiti/chapter6/list',
    method: 'get',
    params: query
  })
}
// demo6 任务查询
export function taskListChapter6(query) {
  return request({
    url: '/activiti/chapter6/task/list',
    method: 'get',
    params: query
  })
}

// demo6 部署流程
export function deployChapter6(query) {
  return request({
    url: '/activiti/chapter6/deploy',
    method: 'get',
    params: query
  })
}

// demo6 删除流程
export function delChapter6(deploymentId) {
  return request({
    url: '/activiti/chapter6/delete/' + deploymentId,
    method: 'delete'
  })
}

// demo6 启动流程
export function startProcessChapter6(data) {
  console.log("datadatadatadatadata")
  console.log(data)
  return request({
    url: '/activiti/chapter6/start',
    method: 'post',
    data: data
  })
}
