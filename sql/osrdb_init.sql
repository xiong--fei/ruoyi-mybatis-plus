/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     2021/3/25 15:09:40                           */
/*==============================================================*/

drop table gen_table cascade constraints;

drop table gen_table_column cascade constraints;

drop table qrtz_blob_triggers cascade constraints;

drop table qrtz_calendars cascade constraints;

drop table qrtz_cron_triggers cascade constraints;

drop table qrtz_fired_triggers cascade constraints;

drop table qrtz_job_details cascade constraints;

drop table qrtz_locks cascade constraints;

drop table qrtz_paused_trigger_grps cascade constraints;

drop table qrtz_scheduler_state cascade constraints;

drop table qrtz_simple_triggers cascade constraints;

drop table qrtz_simprop_triggers cascade constraints;

drop table qrtz_triggers cascade constraints;

drop table sys_config cascade constraints;

drop table sys_dept cascade constraints;

drop table sys_dict_data cascade constraints;

drop table sys_dict_type cascade constraints;

drop table sys_job cascade constraints;

drop table sys_job_log cascade constraints;

drop table sys_logininfor cascade constraints;

drop table sys_menu cascade constraints;

drop table sys_notice cascade constraints;

drop table sys_oper_log cascade constraints;

drop table sys_post cascade constraints;

drop table sys_role cascade constraints;

drop table sys_role_dept cascade constraints;

drop table sys_role_menu cascade constraints;

drop table sys_user cascade constraints;

drop table sys_user_post cascade constraints;

drop table sys_user_role cascade constraints;

/*==============================================================*/
/* Table: gen_table                                           */
/*==============================================================*/
create table gen_table
(
   table_id           NUMBER(22)           not null,
   table_name         VARCHAR2(200),
   table_comment      VARCHAR2(500),
   sub_table_name     VARCHAR2(64),
   sub_table_fk_name  VARCHAR2(64),
   class_name         VARCHAR2(100),
   tpl_category       VARCHAR2(200)        default 'crud',
   package_name       VARCHAR2(100),
   module_name        VARCHAR2(30),
   business_name      VARCHAR2(30),
   function_name      VARCHAR2(50),
   function_author    VARCHAR2(50),
   gen_type           CHAR(1)              default '0',
   gen_path           VARCHAR2(200)        default '/',
   options            VARCHAR2(1000),
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(500),
   constraint PK_GEN_TABLE primary key (table_id)
);

comment on table gen_table is '代码生成业务表';


comment on column gen_table.table_id is '编号';


comment on column gen_table.table_name is '表名称';


comment on column gen_table.table_comment is '表描述';


comment on column gen_table.sub_table_name is '关联子表的表名';


comment on column gen_table.sub_table_fk_name is '子表关联的外键名';


comment on column gen_table.class_name is '实体类名称';


comment on column gen_table.tpl_category is '使用的模板（crud单表操作 tree树表操作）';


comment on column gen_table.package_name is '生成包路径';


comment on column gen_table.module_name is '生成模块名';


comment on column gen_table.business_name is '生成业务名';


comment on column gen_table.function_name is '生成功能名';


comment on column gen_table.function_author is '生成功能作者';


comment on column gen_table.gen_type is '生成代码方式（0zip压缩包 1自定义路径）';


comment on column gen_table.gen_path is '生成路径（不填默认项目路径）';


comment on column gen_table.options is '其它生成选项';


comment on column gen_table.create_by is '创建者';


comment on column gen_table.create_time is '创建时间';


comment on column gen_table.update_by is '更新者';


comment on column gen_table.update_time is '更新时间';


comment on column gen_table.remark is '备注';


/*==============================================================*/
/* Table: gen_table_column                                    */
/*==============================================================*/
create table gen_table_column
(
   column_id          NUMBER(22)           not null,
   table_id           NUMBER(22),
   column_name        VARCHAR2(200),
   column_comment     VARCHAR2(500),
   column_type        VARCHAR2(100),
   java_type          VARCHAR2(500),
   java_field         VARCHAR2(200),
   is_pk              CHAR(1),
   is_increment       CHAR(1),
   is_required        CHAR(1),
   is_insert          CHAR(1),
   is_edit            CHAR(1),
   is_list            CHAR(1),
   is_query           CHAR(1),
   query_type         VARCHAR2(200)        default 'EQ',
   html_type          VARCHAR2(200),
   dict_type          VARCHAR2(200),
   sort               INTEGER,
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   constraint PK_GEN_TABLE_COLUMN primary key (column_id)
);

comment on table gen_table_column is '代码生成业务表字段';


comment on column gen_table_column.table_id is '归属表编号';


comment on column gen_table_column.column_name is '列名称';


comment on column gen_table_column.column_comment is '列描述';


comment on column gen_table_column.column_type is '列类型';


comment on column gen_table_column.java_type is 'JAVA类型';


comment on column gen_table_column.java_field is 'JAVA字段名';


comment on column gen_table_column.is_pk is '是否主键（1是）';


comment on column gen_table_column.is_increment is '是否自增（1是）';


comment on column gen_table_column.is_required is '是否必填（1是）';


comment on column gen_table_column.is_insert is '是否为插入字段（1是）';


comment on column gen_table_column.is_edit is '是否编辑字段（1是）';


comment on column gen_table_column.is_list is '是否列表字段（1是）';


comment on column gen_table_column.is_query is '是否查询字段（1是）';


comment on column gen_table_column.query_type is '查询方式（等于、不等于、大于、小于、范围）';


comment on column gen_table_column.html_type is '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）';


comment on column gen_table_column.dict_type is '字典类型';


comment on column gen_table_column.sort is '排序';


comment on column gen_table_column.create_by is '创建者';


comment on column gen_table_column.create_time is '创建时间';


comment on column gen_table_column.update_by is '更新者';


comment on column gen_table_column.update_time is '更新时间';


/*==============================================================*/
/* Table: qrtz_blob_triggers                                  */
/*==============================================================*/
create table qrtz_blob_triggers
(
   sched_name         VARCHAR2(120)        not null,
   trigger_name       VARCHAR2(200)        not null,
   trigger_group      VARCHAR2(200)        not null,
   blob_data          BLOB,
   constraint PK_QRTZ_BLOB_TRIGGERS primary key (sched_name, trigger_name, trigger_group)
);

/*==============================================================*/
/* Table: qrtz_calendars                                      */
/*==============================================================*/
create table qrtz_calendars
(
   sched_name         VARCHAR2(120)        not null,
   calendar_name      VARCHAR2(200)        not null,
   calendar           BLOB                 not null,
   constraint PK_QRTZ_CALENDARS primary key (sched_name, calendar_name)
);

/*==============================================================*/
/* Table: qrtz_cron_triggers                                  */
/*==============================================================*/
create table qrtz_cron_triggers
(
   sched_name         VARCHAR2(120)        not null,
   trigger_name       VARCHAR2(200)        not null,
   trigger_group      VARCHAR2(200)        not null,
   cron_expression    VARCHAR2(200)        not null,
   time_zone_id       VARCHAR2(80),
   constraint PK_QRTZ_CRON_TRIGGERS primary key (sched_name, trigger_name, trigger_group)
);

/*==============================================================*/
/* Table: qrtz_fired_triggers                                 */
/*==============================================================*/
create table qrtz_fired_triggers
(
   sched_name         VARCHAR2(120)        not null,
   entry_id           VARCHAR2(95)         not null,
   trigger_name       VARCHAR2(200)        not null,
   trigger_group      VARCHAR2(200)        not null,
   instance_name      VARCHAR2(200)        not null,
   fired_time         NUMBER(30)           not null,
   sched_time         NUMBER(30)           not null,
   priority           INTEGER              not null,
   state              VARCHAR2(16)         not null,
   job_name           VARCHAR2(200),
   job_group          VARCHAR2(200),
   is_nonconcurrent   VARCHAR2(1),
   requests_recovery  VARCHAR2(1),
   constraint PK_QRTZ_FIRED_TRIGGERS primary key (sched_name, entry_id)
);

/*==============================================================*/
/* Table: qrtz_job_details                                    */
/*==============================================================*/
create table qrtz_job_details
(
   sched_name         VARCHAR2(120)        not null,
   job_name           VARCHAR2(200)        not null,
   job_group          VARCHAR2(200)        not null,
   description        VARCHAR2(250),
   job_class_name     VARCHAR2(250)        not null,
   is_durable         VARCHAR2(1)          not null,
   is_nonconcurrent   VARCHAR2(1)          not null,
   is_update_data     VARCHAR2(1)          not null,
   requests_recovery  VARCHAR2(1)          not null,
   job_data           BLOB,
   constraint PK_QRTZ_JOB_DETAILS primary key (sched_name, job_name, job_group)
);

/*==============================================================*/
/* Table: qrtz_locks                                          */
/*==============================================================*/
create table qrtz_locks
(
   sched_name         VARCHAR2(120)        not null,
   lock_name          VARCHAR2(40)         not null,
   constraint PK_QRTZ_LOCKS primary key (sched_name, lock_name)
);

/*==============================================================*/
/* Table: qrtz_paused_trigger_grps                            */
/*==============================================================*/
create table qrtz_paused_trigger_grps
(
   sched_name         VARCHAR2(120)        not null,
   trigger_group      VARCHAR2(200)        not null,
   constraint PK_QRTZ_PAUSED_TRIGGER_GRPS primary key (sched_name, trigger_group)
);

/*==============================================================*/
/* Table: qrtz_scheduler_state                                */
/*==============================================================*/
create table qrtz_scheduler_state
(
   sched_name         VARCHAR2(120)        not null,
   instance_name      VARCHAR2(200)        not null,
   last_checkin_time  NUMBER(30)           not null,
   checkin_interval   INTEGER              not null,
   constraint PK_QRTZ_SCHEDULER_STATE primary key (sched_name, instance_name)
);

/*==============================================================*/
/* Table: qrtz_simple_triggers                                */
/*==============================================================*/
create table qrtz_simple_triggers
(
   sched_name         VARCHAR2(120)        not null,
   trigger_name       VARCHAR2(200)        not null,
   trigger_group      VARCHAR2(200)        not null,
   repeat_count       INTEGER              not null,
   repeat_interval    INTEGER              not null,
   times_triggered    INTEGER              not null,
   constraint PK_QRTZ_SIMPLE_TRIGGERS primary key (sched_name, trigger_name, trigger_group)
);

/*==============================================================*/
/* Table: qrtz_simprop_triggers                               */
/*==============================================================*/
create table qrtz_simprop_triggers
(
   sched_name         VARCHAR2(120)        not null,
   trigger_name       VARCHAR2(200)        not null,
   trigger_group      VARCHAR2(200)        not null,
   str_prop_1         VARCHAR2(512),
   str_prop_2         VARCHAR2(512),
   str_prop_3         VARCHAR2(512),
   int_prop_1         NUMBER(20),
   int_prop_2         NUMBER(20),
   long_prop_1        NUMBER(30),
   long_prop_2        NUMBER(30),
   dec_prop_1         NUMBER(13,4),
   dec_prop_2         NUMBER(13,4),
   bool_prop_1        VARCHAR2(1),
   bool_prop_2        VARCHAR2(1),
   constraint PK_QRTZ_SIMPROP_TRIGGERS primary key (sched_name, trigger_name, trigger_group)
);

/*==============================================================*/
/* Table: qrtz_triggers                                       */
/*==============================================================*/
create table qrtz_triggers
(
   sched_name         VARCHAR2(120)        not null,
   trigger_name       VARCHAR2(200)        not null,
   trigger_group      VARCHAR2(200)        not null,
   job_name           VARCHAR2(200)        not null,
   job_group          VARCHAR2(200)        not null,
   description        VARCHAR2(255),
   next_fire_time     NUMBER(18),
   prev_fire_time     NUMBER(18),
   priority           NUMBER(18),
   trigger_state      VARCHAR2(16),
   trigger_type       VARCHAR2(8),
   start_time         NUMBER(30),
   end_time           NUMBER(30),
   calendar_name      VARCHAR2(200),
   misfire_instr      NUMBER(18),
   job_data           BLOB,
   constraint PK_QRTZ_TRIGGERS primary key (sched_name, trigger_name, trigger_group)
);

/*==============================================================*/
/* Table: sys_config                                          */
/*==============================================================*/
create table sys_config
(
   config_id          NUMBER(22)           not null,
   config_name        VARCHAR2(100)        default '0',
   config_key         VARCHAR2(100),
   config_value       VARCHAR2(500),
   config_type        CHAR(1)              default 'N',
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(500),
   constraint PK_SYS_CONFIG primary key (config_id)
);

comment on table sys_config is '参数配置表';


comment on column sys_config.config_id is '字典编码';


comment on column sys_config.config_name is '字典排序';


comment on column sys_config.config_key is '字典标签';


comment on column sys_config.config_value is '字典键值';


comment on column sys_config.config_type is '字典类型';


comment on column sys_config.create_by is '创建者';


comment on column sys_config.create_time is '创建时间';


comment on column sys_config.update_by is '更新者';


comment on column sys_config.update_time is '更新时间';


comment on column sys_config.remark is '备注';


/*==============================================================*/
/* Table: sys_dept                                            */
/*==============================================================*/
create table sys_dept
(
   dept_id            NUMBER(22)           not null,
   parent_id          NUMBER(22)           default 0,
   ancestors          VARCHAR2(50),
   dept_name          VARCHAR2(230),
   order_num          INTEGER              default 0,
   leader             VARCHAR2(20),
   phone              VARCHAR2(20),
   email              VARCHAR2(50),
   status             CHAR(1)              default '0',
   del_flag           CHAR(1)              default '0',
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   constraint PK_SYS_DEPT primary key (dept_id)
);

comment on table sys_dept is '部门表';


comment on column sys_dept.dept_id is '部门id';


comment on column sys_dept.parent_id is '父部门id';

comment on column sys_dept.ancestors is '祖级列表';

comment on column sys_dept.dept_name is '部门名称';


comment on column sys_dept.order_num is '显示顺序';


comment on column sys_dept.leader is '负责人';


comment on column sys_dept.phone is '联系电话';


comment on column sys_dept.email is '邮箱';


comment on column sys_dept.status is '部门状态:0正常,1停用';

comment on column sys_dept.del_flag is '删除标志（0代表存在 1代表删除）';

comment on column sys_dept.create_by is '创建者';


comment on column sys_dept.create_time is '创建时间';


comment on column sys_dept.update_by is '更新者';


comment on column sys_dept.update_time is '更新时间';


/*==============================================================*/
/* Table: sys_dict_data                                       */
/*==============================================================*/
create table sys_dict_data
(
   dict_code          NUMBER(22)           not null,
   dict_sort          INTEGER              default 0,
   dict_label         VARCHAR2(100),
   dict_value         VARCHAR2(100),
   dict_type          VARCHAR2(100),
   css_class          VARCHAR2(100),
   list_class         VARCHAR2(100),
   is_default         CHAR(1)              default 'N',
   status             CHAR(1)              default '0',
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(500),
   constraint PK_SYS_DICT_DATA primary key (dict_code)
);

comment on table sys_dict_data is '字典数据表';


comment on column sys_dict_data.dict_code is '字典编码';


comment on column sys_dict_data.dict_sort is '字典排序';


comment on column sys_dict_data.dict_label is '字典标签';


comment on column sys_dict_data.dict_value is '字典键值';


comment on column sys_dict_data.dict_type is '字典类型';


comment on column sys_dict_data.css_class is '样式属性（其他样式扩展）';


comment on column sys_dict_data.list_class is '表格回显样式';


comment on column sys_dict_data.is_default is '是否默认（Y是 N否）';


comment on column sys_dict_data.status is '状态（0正常 1禁用）';


comment on column sys_dict_data.create_by is '创建者';


comment on column sys_dict_data.create_time is '创建时间';


comment on column sys_dict_data.update_by is '更新者';


comment on column sys_dict_data.update_time is '更新时间';


comment on column sys_dict_data.remark is '备注';


/*==============================================================*/
/* Table: sys_dict_type                                       */
/*==============================================================*/
create table sys_dict_type
(
   dict_id            NUMBER(22)           not null,
   dict_name          VARCHAR2(100),
   dict_type          VARCHAR2(100),
   status             CHAR(1)              default '0',
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(500),
   constraint PK_SYS_DICT_TYPE primary key (dict_id)
);

comment on table sys_dict_type is '字典类型表';


comment on column sys_dict_type.dict_id is '字典主键';


comment on column sys_dict_type.dict_name is '字典名称';


comment on column sys_dict_type.dict_type is '字典类型';


comment on column sys_dict_type.status is '状态（0正常 1禁用）';


comment on column sys_dict_type.create_by is '创建者';


comment on column sys_dict_type.create_time is '创建时间';


comment on column sys_dict_type.update_by is '更新者';


comment on column sys_dict_type.update_time is '更新时间';


comment on column sys_dict_type.remark is '备注';


/*==============================================================*/
/* Table: sys_job                                             */
/*==============================================================*/
create table sys_job
(
   job_id             NUMBER(22)           not null,
   job_name           VARCHAR2(64)         default '' not null,
   job_group          VARCHAR2(64)         default '' not null,
   invoke_target      VARCHAR2(500)        default '',
   cron_expression    VARCHAR2(255)        default '',
   misfire_policy     VARCHAR2(20)         default '3',
   concurrent         CHAR(1)              default '1',
   status             CHAR(1)              default '0',
   create_by          VARCHAR2(64)         default '',
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64)         default '',
   update_time        TIMESTAMP,
   remark             VARCHAR2(500)        default '',
   constraint PK_SYS_JOB primary key (job_id, job_name, job_group)
);

comment on table sys_job is '定时任务调度表';


comment on column sys_job.job_id is '任务ID';


comment on column sys_job.job_name is '任务名称';


comment on column sys_job.job_group is '任务组名';


comment on column sys_job.invoke_target is '任务方法';


comment on column sys_job.cron_expression is 'cron执行表达式';


comment on column sys_job.misfire_policy is '计划执行错误策略（1立即执行 2执行一次 3放弃执行）';


comment on column sys_job.concurrent is '方法参数';


comment on column sys_job.status is '状态（0正常 1暂停）';


comment on column sys_job.create_by is '创建者';


comment on column sys_job.create_time is '创建时间';


comment on column sys_job.update_by is '更新者';


comment on column sys_job.update_time is '更新时间';


comment on column sys_job.remark is '备注信息';


/*==============================================================*/
/* Table: sys_job_log                                         */
/*==============================================================*/
create table sys_job_log
(
   job_log_id         NUMBER(22)           not null,
   job_name           VARCHAR2(64)         not null,
   job_group          VARCHAR2(64)         not null,
   invoke_target      VARCHAR2(500)        not null,
   job_message        VARCHAR2(500),
   status             CHAR(1)              default '0',
   exception_info     VARCHAR2(2000),
   create_time        TIMESTAMP,
   constraint PK_SYS_JOB_LOG primary key (job_log_id)
);

comment on table sys_job_log is '定时任务调度日志表';


comment on column sys_job_log.job_log_id is '任务日志ID';


comment on column sys_job_log.job_name is '任务名称';


comment on column sys_job_log.job_group is '任务组名';


comment on column sys_job_log.invoke_target is '任务方法';


comment on column sys_job_log.job_message is '日志信息';


comment on column sys_job_log.status is '方法参数';


comment on column sys_job_log.exception_info is '异常信息';


comment on column sys_job_log.create_time is '创建时间';


/*==============================================================*/
/* Table: sys_logininfor                                      */
/*==============================================================*/
create table sys_logininfor
(
   info_id            NUMBER(22)           not null,
   user_name          VARCHAR2(50),
   ipaddr             VARCHAR2(128),
   login_location     VARCHAR2(255),
   browser            VARCHAR2(50),
   os                 VARCHAR2(50),
   status             CHAR(1)              default '0',
   msg                VARCHAR2(255),
   login_time         TIMESTAMP,
   constraint PK_SYS_LOGININFOR primary key (info_id)
);

comment on table sys_logininfor is '系统访问记录';


comment on column sys_logininfor.info_id is '访问ID';


comment on column sys_logininfor.user_name is '用户账号';


comment on column sys_logininfor.ipaddr is '登录IP地址';


comment on column sys_logininfor.login_location is '登录地点';


comment on column sys_logininfor.browser is '浏览器类型';


comment on column sys_logininfor.os is '操作系统';


comment on column sys_logininfor.status is '登录状态 0成功 1失败';


comment on column sys_logininfor.msg is '提示消息';


comment on column sys_logininfor.login_time is '访问时间';


/*==============================================================*/
/* Table: sys_menu                                            */
/*==============================================================*/
create table sys_menu
(
   menu_id            NUMBER(22)           not null,
   menu_name          VARCHAR2(50)         not null,
   parent_id          NUMBER(22)           default 0,
   order_num          INTEGER              default 0,
   path               VARCHAR2(200),
   component          VARCHAR2(255),
   is_frame           INTEGER              default 1,
   is_cache           INTEGER              default 0,
   menu_type          CHAR(1),
   visible            CHAR(1)              default '0',
   status             CHAR(1)              default '0',
   perms              VARCHAR2(100),
   icon               VARCHAR2(100)        default '#',
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(500),
   constraint PK_SYS_MENU primary key (menu_id)
);

comment on table sys_menu is '菜单权限表';


comment on column sys_menu.menu_id is '菜单ID';


comment on column sys_menu.menu_name is '菜单名称';


comment on column sys_menu.parent_id is '父菜单ID';


comment on column sys_menu.order_num is '显示顺序';


comment on column sys_menu.path is '路由地址';


comment on column sys_menu.component is '组件路径';


comment on column sys_menu.is_frame is '是否为外链（0是 1否）';


comment on column sys_menu.is_cache is '是否缓存（0缓存 1不缓存）';


comment on column sys_menu.menu_type is '类型:M目录,C菜单,F按钮';


comment on column sys_menu.visible is '菜单状态:0显示,1隐藏';


comment on column sys_menu.status is '菜单状态（0正常 1停用）';


comment on column sys_menu.perms is '权限标识';


comment on column sys_menu.icon is '菜单图标';


comment on column sys_menu.create_by is '创建者';


comment on column sys_menu.create_time is '创建时间';


comment on column sys_menu.update_by is '更新者';


comment on column sys_menu.update_time is '更新时间';


comment on column sys_menu.remark is '备注';


/*==============================================================*/
/* Table: sys_notice                                          */
/*==============================================================*/
create table sys_notice
(
   notice_id          NUMBER(22)           not null,
   notice_title       VARCHAR2(50)         not null,
   notice_type        CHAR(1)              not null,
   notice_content     CLOB,
   status             CHAR(1)              default '0',
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(255),
   constraint PK_SYS_NOTICE primary key (notice_id)
);

comment on table sys_notice is '通知公告表';


comment on column sys_notice.notice_id is '公告ID';


comment on column sys_notice.notice_title is '公告标题';


comment on column sys_notice.notice_type is '公告类型（1通知 2公告）';


comment on column sys_notice.notice_content is '公告内容';


comment on column sys_notice.status is '公告状态（0正常 1关闭）';


comment on column sys_notice.create_by is '创建者';


comment on column sys_notice.create_time is '创建时间';


comment on column sys_notice.update_by is '更新者';


comment on column sys_notice.update_time is '更新时间';


comment on column sys_notice.remark is '备注';


/*==============================================================*/
/* Table: sys_oper_log                                        */
/*==============================================================*/
create table sys_oper_log
(
   oper_id            NUMBER(22)           not null,
   title              VARCHAR2(50),
   business_type      INTEGER              default 0,
   method             VARCHAR2(100),
   request_method     VARCHAR2(10),
   operator_type      INTEGER              default 0,
   oper_name          VARCHAR2(50),
   dept_name          VARCHAR2(50),
   oper_url           VARCHAR2(255),
   oper_ip            VARCHAR2(128),
   oper_location      VARCHAR2(255),
   oper_param         VARCHAR2(2000),
   json_result        VARCHAR2(2000),
   status             INTEGER              default 0,
   error_msg          VARCHAR2(2000),
   oper_time          TIMESTAMP,
   constraint PK_SYS_OPER_LOG primary key (oper_id)
);

comment on table sys_oper_log is '操作日志记录';


comment on column sys_oper_log.oper_id is '日志主键';


comment on column sys_oper_log.title is '模块标题';


comment on column sys_oper_log.business_type is '业务类型（0其它 1新增 2修改 3删除）';


comment on column sys_oper_log.method is '方法名称';


comment on column sys_oper_log.request_method is '请求方式';


comment on column sys_oper_log.operator_type is '操作类别（0其它 1后台用户 2手机端用户）';


comment on column sys_oper_log.oper_name is '操作人员';


comment on column sys_oper_log.dept_name is '部门名称';


comment on column sys_oper_log.oper_url is '请求URL';


comment on column sys_oper_log.oper_ip is '主机地址';


comment on column sys_oper_log.oper_location is '操作地点';


comment on column sys_oper_log.oper_param is '请求参数';


comment on column sys_oper_log.json_result is '返回参数';


comment on column sys_oper_log.status is '操作状态 0正常 1异常';


comment on column sys_oper_log.error_msg is '错误消息';


comment on column sys_oper_log.oper_time is '操作时间';


/*==============================================================*/
/* Table: sys_post                                            */
/*==============================================================*/
create table sys_post
(
   post_id            NUMBER(22)           not null,
   post_code          VARCHAR2(64)         not null,
   post_name          VARCHAR2(50)         not null,
   post_sort          INTEGER              not null,
   status             CHAR(1)              not null,
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(500),
   constraint PK_SYS_POST primary key (post_id)
);

comment on table sys_post is '岗位信息表';


comment on column sys_post.post_id is '岗位ID';


comment on column sys_post.post_code is '岗位编码';


comment on column sys_post.post_name is '岗位名称';


comment on column sys_post.post_sort is '显示顺序';


comment on column sys_post.status is '状态（0正常 1停用）';


comment on column sys_post.create_by is '创建者';


comment on column sys_post.create_time is '创建时间';


comment on column sys_post.update_by is '更新者';


comment on column sys_post.update_time is '更新时间';


comment on column sys_post.remark is '备注';


/*==============================================================*/
/* Table: sys_role                                            */
/*==============================================================*/
create table sys_role
(
   role_id            NUMBER(22)           not null,
   role_name          VARCHAR2(30)         not null,
   role_key           VARCHAR2(100)        not null,
   role_sort          INTEGER              not null,
   data_scope         CHAR(1)              default '1',
   menu_check_strictly SMALLINT             default 1,
   dept_check_strictly SMALLINT             default 1,
   status             CHAR(1)              not null,
   del_flag           CHAR(1)              default '0',
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(500),
   constraint PK_SYS_ROLE primary key (role_id)
);

comment on table sys_role is '角色信息表';


comment on column sys_role.role_id is '角色ID';


comment on column sys_role.role_name is '角色名称';


comment on column sys_role.role_key is '角色权限字符串';


comment on column sys_role.role_sort is '显示顺序';


comment on column sys_role.data_scope is '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）';


comment on column sys_role.menu_check_strictly is '菜单树选择项是否关联显示';


comment on column sys_role.dept_check_strictly is '部门树选择项是否关联显示';


comment on column sys_role.status is '角色状态:0正常,1禁用';


comment on column sys_role.del_flag is '删除标志（0代表存在 1代表删除）';


comment on column sys_role.create_by is '创建者';


comment on column sys_role.create_time is '创建时间';


comment on column sys_role.update_by is '更新者';


comment on column sys_role.update_time is '更新时间';


comment on column sys_role.remark is '备注';


/*==============================================================*/
/* Table: sys_role_dept                                       */
/*==============================================================*/
create table sys_role_dept
(
   role_id            NUMBER(22)           not null,
   dept_id            NUMBER(22)           not null,
   constraint PK_SYS_ROLE_DEPT primary key (role_id, dept_id)
);

comment on table sys_role_dept is '角色和部门关联表';


comment on column sys_role_dept.role_id is '用户ID';


comment on column sys_role_dept.dept_id is '岗位ID';


/*==============================================================*/
/* Table: sys_role_menu                                       */
/*==============================================================*/
create table sys_role_menu
(
   role_id            NUMBER(22)           not null,
   menu_id            NUMBER(22)           not null,
   constraint PK_SYS_ROLE_MENU primary key (role_id, menu_id)
);

comment on table sys_role_menu is '角色和菜单关联表';


comment on column sys_role_menu.role_id is '角色ID';


comment on column sys_role_menu.menu_id is '菜单ID';


/*==============================================================*/
/* Table: sys_user                                            */
/*==============================================================*/
create table sys_user
(
   user_id            NUMBER(22)           not null,
   dept_id            NUMBER(22),
   user_name          VARCHAR2(30),
   nick_name          VARCHAR2(30),
   user_type          VARCHAR2(2)          default '00',
   email              VARCHAR2(50),
   phonenumber        VARCHAR2(11),
   sex                CHAR(1)              default '0',
   avatar             VARCHAR2(100),
   password           VARCHAR2(100),
   status             CHAR(1)              default '0',
   del_flag           CHAR(1)              default '0',
   login_ip           VARCHAR2(128),
   login_date         TIMESTAMP,
   create_by          VARCHAR2(64),
   create_time        TIMESTAMP,
   update_by          VARCHAR2(64),
   update_time        TIMESTAMP,
   remark             VARCHAR2(500),
   constraint PK_SYS_USER primary key (user_id)
);

comment on table sys_user is '用户信息表';


comment on column sys_user.user_id is '用户ID';


comment on column sys_user.dept_id is '部门ID';


comment on column sys_user.user_name is '用户昵称';


comment on column sys_user.nick_name is '用户昵称';


comment on column sys_user.user_type is '类型:Y默认用户,N非默认用户';


comment on column sys_user.email is '用户邮箱';


comment on column sys_user.phonenumber is '手机号码';


comment on column sys_user.sex is '用户性别（0男 1女 2未知）';


comment on column sys_user.avatar is '头像地址';


comment on column sys_user.password is '密码';


comment on column sys_user.status is '帐号状态:0正常,1禁用';


comment on column sys_user.del_flag is '拒绝登录描述';


comment on column sys_user.login_ip is '最后登录IP';


comment on column sys_user.login_date is '最后登录时间';


comment on column sys_user.create_by is '创建者';


comment on column sys_user.create_time is '创建时间';


comment on column sys_user.update_by is '更新者';


comment on column sys_user.update_time is '更新时间';


/*==============================================================*/
/* Table: sys_user_post                                       */
/*==============================================================*/
create table sys_user_post
(
   user_id            NUMBER(22)           not null,
   post_id            NUMBER(22)           not null,
   constraint PK_SYS_USER_POST primary key (user_id, post_id)
);

comment on table sys_user_post is '用户与岗位关联表';


comment on column sys_user_post.user_id is '用户ID';


comment on column sys_user_post.post_id is '岗位ID';


/*==============================================================*/
/* Table: sys_user_role                                       */
/*==============================================================*/
create table sys_user_role
(
   user_id            NUMBER(22)           not null,
   role_id            NUMBER(22)           not null,
   constraint PK_SYS_USER_ROLE primary key (user_id, role_id)
);

comment on table sys_user_role is '用户和角色关联表';

comment on column sys_user_role.user_id is '用户ID';

comment on column sys_user_role.role_id is '角色ID';
