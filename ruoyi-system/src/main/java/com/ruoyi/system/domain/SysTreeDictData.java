package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;
import lombok.Data;

/**
 * 树形字典数据对象 SYS_TREE_DICT_DATA
 *
 * @author fansd
 * @date 2021-05-31
 */

@Data
public class SysTreeDictData extends TreeEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 树形类别
     */
    @Excel(name = "树形类别")
    private String treeDict;

    /**
     * 分类名称
     */
    @Excel(name = "分类名称")
    private String label;

    /**
     * 路径
     */
    @Excel(name = "路径")
    private String path;

    /**
     * 图标地址
     */
    @Excel(name = "图标地址")
    private String icon;

    /**
     * 字典层次码 + 编码的 初始值，默认值001
     */
//    @Excel(name = "字典层次码 + 编码的 初始值")
//    private String layerCodeInitVal;
//
    /**
     * 字典层次码 + 编码的 长度
     */
//    @Excel(name = "字典层次码 + 编码的 长度")
//    private String layerCodeLen;
}
