package com.ruoyi.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.SysConfig;
import com.ruoyi.system.domain.SysOperLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 参数配置 数据层
 *
 * @author ruoyi
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {
}
