package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ruoyi.system.domain.SysTreeDict;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统树形Mapper接口
 *
 * @author fansd
 * @date 2021-06-04
 */
public interface SysTreeDictMapper extends BaseMapper<SysTreeDict> {
    /**
     * 分页查询系统树形列表
     * <p>
     * param page             分页信息
     * param entity           实体
     *
     * @param sysTreeDict 系统树形信息
     * @return 系统树形集合
     */
    public IPage<SysTreeDict> selectTreeDictPage(IPage<SysTreeDict> page, @Param("entity") SysTreeDict sysTreeDict);

    /**
     * 查询所有系统树形列表
     *
     * @param entity 系统树形信息
     * @return 系统树形集合
     */
    public List<SysTreeDict> selectTreeDictList(@Param("entity") SysTreeDict entity);
}
