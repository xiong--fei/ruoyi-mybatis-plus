package com.ruoyi.web.controller.system;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.system.domain.SysTreeDict;
import com.ruoyi.system.service.ISysTreeDictService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 系统树形Controller
 *
 * @author fansd
 * @date 2021-06-04
 */
@RestController
@RequestMapping("system/dict/tree")
public class SysTreeDictController extends BaseController {
    @Autowired
    private ISysTreeDictService SysTreeDictService;

    /**
     * 分页查询系统树形列表
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysTreeDict sysTreeDict) {
        IPage<SysTreeDict> list = SysTreeDictService.selectList(startPage(), sysTreeDict);
        return getDataTable(list);
    }

    /**
     * 导出系统树形列表
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:export')")
    @Log(title = "系统树形", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysTreeDict sysTreeDict) {
        List<SysTreeDict> list = SysTreeDictService.selectListAll(sysTreeDict);
        ExcelUtil<SysTreeDict> util = new ExcelUtil<SysTreeDict>(SysTreeDict.class);
        return util.exportExcel(list, "系统树形");
    }

    /**
     * 新增系统树形
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:add')")
    @Log(title = "系统树形", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysTreeDict sysTreeDict) {
        return toAjax(SysTreeDictService.insert(sysTreeDict));
    }

    /**
     * 修改系统树形
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:edit')")
    @Log(title = "系统树形", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTreeDict sysTreeDict) {
        return toAjax(SysTreeDictService.update(sysTreeDict));
    }

    /**
     * 删除系统树形
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:remove')")
    @Log(title = "系统树形", businessType = BusinessType.DELETE)
    @DeleteMapping("/{sids}")
    public AjaxResult remove(@PathVariable String[] sids) {
        return toAjax(SysTreeDictService.deleteByIds(sids));
    }
}
