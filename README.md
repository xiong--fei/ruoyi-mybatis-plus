## 为了简化开发，增加内容如下：
````
Lombok能以简单的注解形式来简化java代码，提高开发效率
增加mybatis插件 baomidou

Mybatis 自动填充功能 MyMetaObjectHandler.java
MybatisPlus配置文件 MybatisPlusConfig.java

初步适配国产数据库
osrdb_init.sql 达梦初始化文件
dm_init.sql 神通初始化文件

````
## 平台简介
若依是一套全部开源的快速开发平台，毫无保留给个人及企业免费使用。

* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。
* 提供了单应用版本[RuoYi-Vue-fast](https://github.com/yangzongzhuan/RuoYi-Vue-fast)，Oracle版本[RuoYi-Vue-Oracle](https://github.com/yangzongzhuan/RuoYi-Vue-Oracle)，保持同步更新。
* 不分离版本，请移步[RuoYi](https://gitee.com/y_project/RuoYi)，微服务版本，请移步[RuoYi-Cloud](https://gitee.com/y_project/RuoYi-Cloud)
* 特别鸣谢：[element](https://github.com/ElemeFE/element)，[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)，[eladmin-web](https://github.com/elunez/eladmin-web)。
* 阿里云折扣场：[点我进入](http://aly.ruoyi.vip)，腾讯云秒杀场：[点我进入](http://txy.ruoyi.vip)&nbsp;&nbsp;
* 阿里云优惠券：[点我领取](https://www.aliyun.com/minisite/goods?userCode=brki8iof&share_source=copy_link)，腾讯云优惠券：[点我领取](https://cloud.tencent.com/redirect.php?redirect=1025&cps_key=198c8df2ed259157187173bc7f4f32fd&from=console)&nbsp;&nbsp;

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 缓存监控：对系统的缓存信息查询，命令统计等。
17. 在线构建器：拖动表单元素生成相应的HTML代码。
18. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 在线体验

- admin/admin123  
- 陆陆续续收到一些打赏，为了更好的体验已用于演示服务器升级。谢谢各位小伙伴。

演示地址：http://vue.ruoyi.vip  
文档地址：http://doc.ruoyi.vip

## 演示图

<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/cd1f90be5f2684f4560c9519c0f2a232ee8.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/1cbcf0e6f257c7d3a063c0e3f2ff989e4b3.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8074972883b5ba0622e13246738ebba237a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-9f88719cdfca9af2e58b352a20e23d43b12.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-39bf2584ec3a529b0d5a3b70d15c9b37646.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-936ec82d1f4872e1bc980927654b6007307.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-b2d62ceb95d2dd9b3fbe157bb70d26001e9.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d67451d308b7a79ad6819723396f7c3d77a.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/5e8c387724954459291aafd5eb52b456f53.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/644e78da53c2e92a95dfda4f76e6d117c4b.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8370a0d02977eebf6dbf854c8450293c937.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-49003ed83f60f633e7153609a53a2b644f7.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d4fe726319ece268d4746602c39cffc0621.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c195234bbcd30be6927f037a6755e6ab69c.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/b6115bc8c31de52951982e509930b20684a.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-5e4daac0bb59612c5038448acbcef235e3a.png"/></td>
    </tr>
</table>

## 私有jar包安装方法
1. 添加达梦到maven库（DM7驱动程序路径：project_path/doc/resource/jar/)

   ````
    mvn install:install-file -Dfile=D:\xxxx\Dm7JdbcDriver15.jar -DgroupId=com.dm -DartifactId=dmJDBC -Dversion=7.6.15 -Dpackaging=jar
    mvn install:install-file -Dfile=D:\xxxx\Dm7JdbcDriver16.jar -DgroupId=com.dm -DartifactId=dmJDBC -Dversion=7.6.16 -Dpackaging=jar
    mvn install:install-file -Dfile=D:\xxxx\Dm7JdbcDriver17.jar -DgroupId=com.dm -DartifactId=dmJDBC -Dversion=7.6.17 -Dpackaging=jar
    mvn install:install-file -Dfile=D:\xxxx\Dm7JdbcDriver18.jar -DgroupId=com.dm -DartifactId=dmJDBC -Dversion=7.6.18 -Dpackaging=jar


## PDM使用规则
1. 在workspace中找到所需physical data,设计package和table
2. 设计package， 选择 physical diagram 右键属性，
	2.1. name属性设计中文包名， 规则使用下划线分割进行命名
	2.1. comment属性设计中文包名
	2.2. code属性设计实际包名例如:com.ruoyi.***
3. 设计table
	3.1. comment属性设计中文表名（实体名）
	3.2. code属性设计英文表名（实体名）
4. 设计column
	4.1：name和code属性保持一致，字段名（实体字段）， 规则使用下划线分割进行命名
	4.2：comment属性设计中文属性名（注解中使用）
	4.3: 主键选择“P”
	4.3：非空校验选择“M”
	4.4：主键统一使用ID来表示，不建议使用每张表都是用特定的主键标识；主键使用varchar64，不建议使用long或number等类型，容易造成axio.js 解析失真
	4.5：外键统一使用***_ID表示， 字段类型使用varchar(64)（与主表统一）
	4.5：字典数据使用char(2)类型表示
	4.7：如有默认值，在Default Value中设置

## 自动生成代码
	1. 配置文件位置：***\ruoyi-generator\src\main\resourcesgenerator.yml
	2. 根据配置文件中的内容配置，下面配置为2个关键指标
		pdmPath：PDM文件的绝对路径
		exclusionTablePrefix： 需要排除的表

## spring相关
    1. 在没有@Component和@Service的情况下，可以通过SpringUtils.getBean(RedisCache.class)获取实例（RedisCache可变）


## 代码规范

1. 字段类型规范
    ````
   主键：id                        varchar(100)
   父类：pid                       varchar(100)
   外键：**_id                     varchar(100)
   名称：name                      varchar(128)
   备注：remark                    varchar(255)
   租户：TENANT_ID                 varchar(64)
   创建人：CREATE_BY               varchar(64)
   创建时间：CREATE_DATE           timestamp
   更新人：UPDATE_BY               varchar(64)
   更新时间：UPDATE_DATE           timestamp

## 常用类说明

1. 常量类
    ````
    1. CharConstants
    2. Constants
    3. FileConstants
    4. GenConstants
    5. HttpStatus
    6. ScheduleConstants
    7. SqlConstants
    8. TreeConstants
    9. UserConstants
2. 常用配置类
    ````
   1. RedisCache: redis配置类
3. 备注
    ````
